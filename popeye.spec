%define debug_package %{nil}

Name:           popeye
Version:        0.21.5
Release:        0%{?dist}
Summary:        A Kubernetes cluster resource sanitizer

License:        ASL 2.0
URL:            https://popeyecli.io/
Source0:        https://github.com/derailed/popeye/releases/download/v%{version}/popeye_linux_amd64.tar.gz

%description
A Kubernetes cluster resource sanitizer

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
install -p -m 755 %{name} %{buildroot}/usr/bin

%files
/usr/bin/popeye

%changelog
* Thu Apr 11 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version v0.21.5

* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v0.20.4

* Thu Dec 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.11.2

* Wed Feb 22 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.11.1

* Thu Sep 11 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Inital RPM
